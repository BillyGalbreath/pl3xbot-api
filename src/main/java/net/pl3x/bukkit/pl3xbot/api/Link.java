package net.pl3x.bukkit.pl3xbot.api;

import java.util.UUID;

/**
 * Linked account information
 */
public class Link {
    private final String discordID;
    private final UUID bukkitUUID;

    /**
     * Create a link object
     *
     * @param discordID  Discord ID
     * @param bukkitUUID Bukkit UUID
     */
    public Link(String discordID, UUID bukkitUUID) {
        this.discordID = discordID;
        this.bukkitUUID = bukkitUUID;
    }

    /**
     * Get the Discord ID
     *
     * @return Discord ID
     */
    public String getDiscordID() {
        return discordID;
    }

    /**
     * Get the Bukkit UUID
     *
     * @return Bukkit UUID
     */
    public UUID getBukkitUUID() {
        return bukkitUUID;
    }
}
