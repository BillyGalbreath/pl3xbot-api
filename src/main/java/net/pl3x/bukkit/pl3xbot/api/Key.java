package net.pl3x.bukkit.pl3xbot.api;

/**
 * Secret key for linking discord and minecraft accounts
 */
public class Key {
    public final static long MINUTE_MILLIS = 60000;

    private final String id;
    private final String key;
    private final long expire;

    /**
     * Secret key for linking accounts
     *
     * @param id     Discord ID this key belongs to
     * @param key    Secret key
     * @param expire Expiration date
     */
    public Key(String id, String key, long expire) {
        this.id = id;
        this.key = key;
        this.expire = expire;
    }

    /**
     * Get the discord ID this key belongs to
     *
     * @return Discord ID
     */
    public String getId() {
        return id;
    }

    /**
     * Get the secret key
     *
     * @return Secret key
     */
    public String getKey() {
        return key;
    }

    /**
     * Get the expiration date
     * <p>
     * Date is in milliseconds since epoch
     *
     * @return Expiration date
     */
    public long getExpire() {
        return expire;
    }

    /**
     * Check if key has expired
     *
     * @return True if key is expired
     */
    public boolean isExpired() {
        return expire < System.currentTimeMillis();
    }

    /**
     * Get the number of minutes left until expiration
     * <p>
     * This number is rounded using Math#round()
     *
     * @return Number of minutes until key expires
     */
    public int getTimeLeft() {
        return Math.round((expire - System.currentTimeMillis()) / Key.MINUTE_MILLIS);
    }
}
