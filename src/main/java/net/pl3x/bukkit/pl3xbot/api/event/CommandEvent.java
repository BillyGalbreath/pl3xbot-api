package net.pl3x.bukkit.pl3xbot.api.event;

import org.bukkit.entity.Player;
import sx.blah.discord.handle.obj.IMessage;

/**
 * Event triggered when discord command is about to run
 * <p>
 * If cancelled, bot will not perform the command
 */
public class CommandEvent extends BotEvent {
    private final IMessage message;
    private final Player sender;
    private final String command;
    private final String[] args;

    /**
     * Discord command event
     *
     * @param message Discord message that triggered command
     * @param sender  Bukkit player that triggered command
     * @param command Command name that triggered command (alias)
     * @param args    Command arguments
     */
    public CommandEvent(IMessage message, Player sender, String command, String[] args) {
        this.message = message;
        this.sender = sender;
        this.command = command;
        this.args = args;
    }

    /**
     * Get discord message that triggered command
     *
     * @return Discord message, or null if command triggered by Bukkit player
     */
    public IMessage getMessage() {
        return message;
    }

    /**
     * Get Bukkit player that triggered command
     *
     * @return Bukkit player, or null if command triggered from discord
     */
    public Player getSender() {
        return sender;
    }

    /**
     * Get command name that triggered command (alias)
     *
     * @return Command name
     */
    public String getCommand() {
        return command;
    }

    /**
     * Get command arguments
     *
     * @return String array of arguments
     */
    public String[] getArgs() {
        return args;
    }
}
