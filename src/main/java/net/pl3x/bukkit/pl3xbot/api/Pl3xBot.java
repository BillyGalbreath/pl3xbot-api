package net.pl3x.bukkit.pl3xbot.api;

import net.pl3x.bukkit.pl3xbot.api.command.BotCommand;
import org.bukkit.entity.Player;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IUser;

/**
 * Pl3xBot controls
 */
public class Pl3xBot {
    private static IPl3xBot bot;

    /**
     * Set Pl3xBot instance if not already set.
     *
     * @param pl3xBot Pl3xBot instance
     * @return Pl3xBot instance set
     */
    public static IPl3xBot setBot(IPl3xBot pl3xBot) {
        if (bot == null) {
            bot = pl3xBot;
        }
        return getBot();
    }

    /**
     * Get Pl3xBot instance
     *
     * @return Pl3xBot instance, or null if not created yet
     */
    public static IPl3xBot getBot() {
        return bot;
    }

    /**
     * Get the discord client
     *
     * @return Discord client
     */
    public static IDiscordClient getClient() {
        return bot.getClient();
    }

    /**
     * Get the discord channel bot is listening/talking to
     *
     * @return Discord channel
     */
    public static IChannel getChannel() {
        return bot.getChannel();
    }

    /**
     * Set the discord channel bot is listening/talking to
     *
     * @param channel Discord channel
     */
    public static void setChannel(IChannel channel) {
        bot.setChannel(channel);
    }

    /**
     * Check if discord user has administrative rights.
     * <p>
     * Checks linked mc account's permission nodes
     *
     * @param author Discord user
     * @return True if can administrate bot
     */
    public static boolean isAdmin(IUser author) {
        return bot.isAdmin(author);
    }

    /**
     * Register new bot command
     *
     * @param command Bot command
     */
    public static void registerCommand(BotCommand command) {
        bot.registerCommand(command);
    }

    /**
     * Connect the bot to discord
     *
     * @return True if successful
     */
    public static boolean connect() {
        return bot.connect();
    }

    /**
     * Disconnect the bot from discord
     *
     * @return True if successful
     */
    public static boolean disconnect() {
        return bot.disconnect();
    }

    /**
     * Reconnect the bot to discord
     */
    public static void reconnect() {
        bot.reconnect();
    }

    /**
     * Send discord user a private message
     *
     * @param user    Discord user
     * @param message Message to send
     */
    public static void privateMessage(IUser user, String message) {
        bot.privateMessage(user, message);
    }

    /**
     * Send minecraft player a private message
     *
     * @param player  Minecraft player
     * @param message Message to send
     */
    public static void privateMessage(Player player, String message) {
        bot.privateMessage(player, message);
    }

    /**
     * Send chat message to discord and minecraft
     * <p>
     * i.e., bot chatting as someone else (fake forward)
     *
     * @param sender  Name of chat sender
     * @param message Chat message to send
     */
    public static void sendToDiscordAndMinecraft(String sender, String message) {
        bot.sendToDiscordAndMinecraft(sender, message);
    }

    /**
     * Send message to discord and minecraft
     * <p>
     * i.e., bot responding to command
     *
     * @param message Message to send
     */
    public static void sendToDiscordAndMinecraft(String message) {
        bot.sendToDiscordAndMinecraft(message);
    }

    /**
     * Send chat message to minecraft
     * <p>
     * i.e., bot forwarding someone's chat to minecraft
     *
     * @param sender  Name of chat sender
     * @param message Chat message to send
     */
    public static void sendToMinecraft(String sender, String message) {
        bot.sendToMinecraft(sender, message);
    }

    /**
     * Send message to minecraft
     * <p>
     * i.e., bot appearing to chat in minecraft
     *
     * @param message Message to send
     */
    public static void sendToMinecraft(String message) {
        bot.sendToMinecraft(message);
    }

    /**
     * Send chat message to discord
     * <p>
     * i.e., bot forwarding someone's chat to discord
     *
     * @param sender  Name of chat sender
     * @param message Chat message to send
     */
    public static void sendToDiscord(String sender, String message) {
        bot.sendToDiscord(sender, message);
    }

    /**
     * Send message to discord
     * <p>
     * i.e., bot appearing to chat in discord
     *
     * @param message Message to send
     */
    public static void sendToDiscord(String message) {
        bot.sendToDiscord(message);
    }
}
