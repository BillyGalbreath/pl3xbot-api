package net.pl3x.bukkit.pl3xbot.api;

import net.pl3x.bukkit.pl3xbot.api.command.BotCommand;
import org.bukkit.entity.Player;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IUser;

/**
 * IPl3xBot controls
 */
public abstract class IPl3xBot {
    /**
     * Get the discord client
     *
     * @return Discord client
     */
    public abstract IDiscordClient getClient();

    /**
     * Get the discord channel bot is listening/talking to
     *
     * @return Discord channel
     */
    public abstract IChannel getChannel();

    /**
     * Set the discord channel bot is listening/talking to
     *
     * @param channel Discord channel
     */
    public abstract void setChannel(IChannel channel);

    /**
     * Check if discord user has administrative rights.
     * <p>
     * Checks linked mc account's permission nodes
     *
     * @param author Discord user
     * @return True if can administrate bot
     */
    public abstract boolean isAdmin(IUser author);

    /**
     * Register new bot command
     *
     * @param command Bot command
     */
    public abstract void registerCommand(BotCommand command);

    /**
     * Connect the bot to discord
     *
     * @return True if successful
     */
    public abstract boolean connect();

    /**
     * Disconnect the bot from discord
     *
     * @return True if successful
     */
    public abstract boolean disconnect();

    /**
     * Reconnect the bot to discord
     */
    public abstract void reconnect();

    /**
     * Send discord user a private message
     *
     * @param user    Discord user
     * @param message Message to send
     */
    public abstract void privateMessage(IUser user, String message);

    /**
     * Send minecraft player a private message
     *
     * @param player  Minecraft player
     * @param message Message to send
     */
    public abstract void privateMessage(Player player, String message);

    /**
     * Send chat message to discord and minecraft
     * <p>
     * i.e., bot chatting as someone else (fake forward)
     *
     * @param sender  Name of chat sender
     * @param message Chat message to send
     */
    public abstract void sendToDiscordAndMinecraft(String sender, String message);

    /**
     * Send message to discord and minecraft
     * <p>
     * i.e., bot responding to command
     *
     * @param message Message to send
     */
    public abstract void sendToDiscordAndMinecraft(String message);

    /**
     * Send chat message to minecraft
     * <p>
     * i.e., bot forwarding someone's chat to minecraft
     *
     * @param sender  Name of chat sender
     * @param message Chat message to send
     */
    public abstract void sendToMinecraft(String sender, String message);

    /**
     * Send message to minecraft
     * <p>
     * i.e., bot appearing to chat in minecraft
     *
     * @param message Message to send
     */
    public abstract void sendToMinecraft(String message);

    /**
     * Send chat message to discord
     * <p>
     * i.e., bot forwarding someone's chat to discord
     *
     * @param sender  Name of chat sender
     * @param message Chat message to send
     */
    public abstract void sendToDiscord(String sender, String message);

    /**
     * Send message to discord
     * <p>
     * i.e., bot appearing to chat in discord
     *
     * @param message Message to send
     */
    public abstract void sendToDiscord(String message);
}
