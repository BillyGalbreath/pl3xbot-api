package net.pl3x.bukkit.pl3xbot.api.command;

import java.util.Collection;
import org.bukkit.entity.Player;
import sx.blah.discord.handle.obj.IMessage;

/**
 * Manage bot commands
 */
public abstract class CommandManager {
    private static CommandManager manager;

    /**
     * Get the bot command manager instance
     *
     * @return Bot command manager instance
     */
    public static CommandManager getManager() {
        return manager;
    }

    /**
     * Set the bot's command manager if not already set
     *
     * @param manager Command manager
     * @return The set command manager (may not be the one fed as param)
     */
    public static CommandManager setManager(CommandManager manager) {
        if (CommandManager.manager == null) {
            CommandManager.manager = manager;
        }
        return manager;
    }

    /**
     * Register a bot command
     *
     * @param command BotCommand to register
     */
    public abstract void registerCommand(BotCommand command);

    /**
     * Run a bot command that was triggered from discord
     *
     * @param message Discord message that triggered command
     */
    public abstract void runCommand(IMessage message);

    /**
     * Run bot command that was triggered from minecraft
     *
     * @param sender  Bukkit player that triggered command
     * @param message The chat message that triggered command
     */
    public abstract void runCommand(Player sender, String message);

    /**
     * Get the names of all registered commands
     *
     * @return All registered command names
     */
    public abstract Collection<String> getRegisteredCommands();

    /**
     * Get a BotCommand by command name or alias
     *
     * @param name Command name or alias
     * @return BotCommand if found, null if none found
     */
    public abstract BotCommand getCommand(String name);
}
