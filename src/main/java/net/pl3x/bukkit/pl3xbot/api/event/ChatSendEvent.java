package net.pl3x.bukkit.pl3xbot.api.event;

import org.bukkit.entity.Player;

/**
 * Event triggered when minecraft sends chat message to discord
 * <p>
 * If cancelled, bot will not forward the chat to discord
 */
public class ChatSendEvent extends MessageSendEvent {
    private final Player sender;

    /**
     * Message send to discord event
     *
     * @param sender  Bukkit sender
     * @param message Message sent
     */
    public ChatSendEvent(Player sender, String message) {
        super(message);
        this.sender = sender;
    }

    /**
     * Get Bukkit player that sent message
     *
     * @return Bukkit player
     */
    public Player getSender() {
        return sender;
    }
}
