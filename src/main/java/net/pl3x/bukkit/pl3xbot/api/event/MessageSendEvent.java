package net.pl3x.bukkit.pl3xbot.api.event;

/**
 * Event triggered when minecraft sends message to discord
 * <p>
 * i.e., death, login, quit messages, etc.
 * <p>
 * If cancelled, bot will not forward the message to discord
 */
public class MessageSendEvent extends BotEvent {
    private String message;

    /**
     * Minecraft message sent to discord event
     *
     * @param message Message sent
     */
    public MessageSendEvent(String message) {
        this.message = message;
    }

    /**
     * Get the message sent to discord
     *
     * @return Message sent
     */
    public String getMessage() {
        return message;
    }

    /**
     * Set the message to send to discord
     *
     * @param message Message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }
}
