package net.pl3x.bukkit.pl3xbot.api.command;

import java.util.Collection;
import org.bukkit.entity.Player;
import sx.blah.discord.handle.obj.IMessage;

/**
 * Bot command executor
 */
public interface BotCommand {
    /**
     * Get command name
     *
     * @return Name of command
     */
    String getName();

    /**
     * Get command aliases
     *
     * @return String list of aliases
     */
    Collection<String> getAliases();

    /**
     * Run command
     *
     * @param message Discord message from discord
     * @param sender  Bukkit player
     * @param label   Command or alias that triggered command
     * @param args    Command arguments
     */
    void runCommand(IMessage message, Player sender, String label, String[] args);
}
