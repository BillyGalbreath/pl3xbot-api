package net.pl3x.bukkit.pl3xbot.api.event;

import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;

/**
 * Event triggered when minecraft receives chat message from discord
 * <p>
 * If cancelled, bot will not forward the chat to minecraft
 */
public class ChatReceiveEvent extends BotEvent {
    private final IMessage message;
    private final String formatted;

    /**
     * Message received from discord event
     *
     * @param message   Discord message received
     * @param formatted Formatted message received
     */
    public ChatReceiveEvent(IMessage message, String formatted) {
        this.message = message;
        this.formatted = formatted;
    }

    /**
     * Get the discord message received from discord
     * <p>
     * This includes all the raw objects included with discord messages
     *
     * @return IMessage received from discord
     */
    public IMessage getMessage() {
        return message;
    }

    /**
     * Get the formatted message received from discord
     * <p>
     * This means all the discord objects have been removed, including all mentions and roles reformatting to strings
     *
     * @return Formatted message received from discord
     */
    public String getFormattedMessage() {
        return formatted;
    }

    /**
     * Get the discord user that sent this message
     *
     * @return Discord user
     */
    public IUser getSender() {
        return message.getAuthor();
    }
}
